const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String //Google Login: No longer required			
	},
	categories : [
		{		
			category: {
				type: String,
				required: [true, "Category is required."]
			},	
			categoryType: {
				type: String,
				required: [true, "Type is required"]
			}			
		}
	],
	records: [

		{
			categoryType: {
				type: String,
				required: [true, "Category Type is required"]
			},
			categoryName: {
				type: String,
				required: [true, "Category Name is required"]
			},
			amount: {
				type: Number,
				required:[true, "Amount is required"]
			},
			description: {
				type: String,
				required: [true, "Description is required"]
			},
			date: {
				type: Date,
				default: new Date()
			},
			balance: {
				type: Number
			}

		}
	],
	//userdocument -id
		//record object - id
	

/*	amount: x = amount-balance
			id:
			cubao:
			balance: x 

	income:
	amount:500
		id:
		mannequin:
		balance: 500*/

	loginType: {
		type: String,
		required: [true, 'Login Type is required']
	},

})

module.exports = mongoose.model('user', userSchema)



