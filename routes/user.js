const User = require("../models/User")
//With module exports the user.js from models can be called as part of the require
const UserController = require("../controllers/user")
const express = require("express")
const router = express.Router()
const auth = require("../auth")

router.get('/', (req,res)=> {

	UserController.getAllUsers().then(resultFromGetAllUsers => res.send(resultFromGetAllUsers))
})


router.get('/details', auth.verify, (req,res)=> {

	const user = auth.decode(req.headers.authorization)//decoding data
	
	UserController.getUser({userId: user.id}).then(user=> res.send(user))

})


router.get('/get-all', auth.verify, (req,res)=> {

	const user = auth.decode(req.headers.authorization)//decoding data
	
	UserController.getAll({userId: user.id}).then(user=> res.send(user))

})

router.get('/get-all-income', auth.verify, (req,res)=> {

	const user = auth.decode(req.headers.authorization)//decoding data
	
	UserController.getAllIncome({userId: user.id}).then(user=> res.send(user))

})

router.get('/get-all-expenses', auth.verify, (req,res)=> {

	const user = auth.decode(req.headers.authorization)//decoding data
	
	UserController.getAllExpenses({userId: user.id}).then(user=> res.send(user))

})









/*router.get('/get-all-expenses', (req,res)=>{

	UserController.getAllExpenses().then(resultFromGetAllExpenses => res.send(resultFromGetAllExpenses))

})*/


router.post('/register', (req,res)=> {

	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))

})


router.post('/email-exists', (req,res)=> {

	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))

})


router.post('/login', (req, res)=>{

	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})


/*router.post('/createCategory', auth.verify, (req,res)=>{

	let userData = auth.decode(req.headers.authorization) //decode results to the data payload in our token.

	let params = {

		userId: userData.id,
		category: userDa
	}

	UserController.enroll(params).then(resultFromEnroll=> res.send(resultFromEnroll))*/

	//console.log(params)

/*})*/


//http:localhost:4000/users/update
router.post('/createcategory', auth.verify, (req, res)=>{
	
	// let newCategory = {
		
	// 	category: req.body.category, 
	// 	categoryType: req.body.categoryType 
	// }

	UserController.createCategory(req.body).then(resultFromCreateCategory => res.send(resultFromCreateCategory))

})


router.post('/createrecord', auth.verify, (req, res)=>{
	
	console.log(req.body)
	UserController.createRecord(req.body).then(resultFromCreateRecord => res.send(resultFromCreateRecord))

})	


router.post('/verify-google-id-token', async (req,res)=>{

	res.send(await UserController.verifyGoogleTokenId(req.body))
})


/*http://localhost:4000*/

//Integration Routes
//Adding an async keyword to a function, creates an asynchronous function.
//await keyword can only be used in an async function.
//JS is by default, synchronous.
//statement1 -> statement2 -> statement3
//When we want to wait for function to resolve itself, we can make it async
//Async
//statement 1 -> async -> statement3

/*router.post('/verify-google-id-token', async (req,res)=>{

	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))

})
*/






module.exports = router